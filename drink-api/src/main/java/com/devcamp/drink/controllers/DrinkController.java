package com.devcamp.drink.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drink.models.Drink;
import com.devcamp.drink.services.DrinkService;

@RestController
@CrossOrigin
public class DrinkController {
    @Autowired
    private DrinkService drinkService;

    @GetMapping("/drink-list")
    public ResponseEntity<List<Drink>> getDrinkList(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {

        try {
            List<Drink> drinkList = drinkService.getAllDrinks(page, size);
            return new ResponseEntity<>(drinkList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @PostMapping("/create-drink")
    public ResponseEntity<Drink> creatDrink(@Valid @RequestBody Drink drink) {

        try {
            System.out.println(drink);
            Drink newDrink = drinkService.createDrink(drink);

            return new ResponseEntity<>(newDrink, HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @GetMapping("/drink/{id}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable("id") String id) {
        try {
            Optional<Drink> drink = drinkService.getDrinkById(id);
            if (drink.isPresent()) {
                return new ResponseEntity<>(drink.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/drink/{id}")
    public ResponseEntity<Drink> updateCVoucherById(@PathVariable("id") String id, @Valid @RequestBody Drink drink) {
        try {
            Drink updatedDrink = drinkService.updateDrink(id, drink);
            return new ResponseEntity<>(updatedDrink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping("/drink/{id}")
    public ResponseEntity<Drink> deleteCVoucherById(@PathVariable("id") String id) {
        try {
            drinkService.deleteDrink(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
