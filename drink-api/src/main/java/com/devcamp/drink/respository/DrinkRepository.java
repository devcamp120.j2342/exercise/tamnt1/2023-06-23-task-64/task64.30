package com.devcamp.drink.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drink.models.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Integer> {

}
