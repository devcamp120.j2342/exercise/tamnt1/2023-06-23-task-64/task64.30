package com.devcamp.drink.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.drink.models.Drink;
import com.devcamp.drink.respository.DrinkRepository;

@Service
public class DrinkService {
    private final DrinkRepository drinkRepository;

    public DrinkService(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    public List<Drink> getAllDrinks(String page, String size) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page<Drink> pageDrink = drinkRepository.findAll(pageable);
        return pageDrink.getContent();
    }

    public Optional<Drink> getDrinkById(String id) {
        int drinkId = Integer.parseInt(id);

        return drinkRepository.findById(drinkId);

    }

    public Drink createDrink(Drink drink) {
        return drinkRepository.save(drink);
    }

    public Drink updateDrink(String id, Drink drink) {
        int drinkId = Integer.parseInt(id);
        Optional<Drink> existingDrinkOptional = drinkRepository.findById(drinkId);
        if (existingDrinkOptional.isPresent()) {
            Drink existingDrink = existingDrinkOptional.get();
            existingDrink.setMaNuocUong(drink.getMaNuocUong());
            existingDrink.setTenNuocUong(drink.getTenNuocUong());
            existingDrink.setDonGia(drink.getDonGia());
            existingDrink.setGhiChu(drink.getGhiChu());

            Drink updatedDrink = drinkRepository.save(existingDrink);
            return updatedDrink;
        } else {

            return null;
        }

    }

    public void deleteDrink(String id) {
        int drinkId = Integer.parseInt(id);
        drinkRepository.deleteById(drinkId);
    }

}
