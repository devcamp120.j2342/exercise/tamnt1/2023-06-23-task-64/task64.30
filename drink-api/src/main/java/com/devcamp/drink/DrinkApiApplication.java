package com.devcamp.drink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DrinkApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinkApiApplication.class, args);
	}

}
